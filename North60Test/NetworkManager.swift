//
//  NetworkManager.swift
//  North60Test
//
//  Created by Minseo Kwon on 2017-12-21.
//  Copyright © 2017 MinKwon. All rights reserved.
//

import Foundation
import Alamofire

class NetworkManager {
    
    static let sharedInstance = NetworkManager()
    let baseURL = "https://lcboapi.com/stores?access_key="
    let consumerKey = "MDo2ODliZTQzYy1lNjdlLTExZTctOTQ4Mi01YjFjZWJiNzVlNjM6WG94dHdBSXBib1JzZ3J6TkV6emlpdkExbGZCQWlqTmZ5MTFq"
    
    func getStores(completion: @escaping ([Store]) -> ()) {
        var stores = [Store]()
        let storeURL = URL(string:baseURL + consumerKey)
        
        Alamofire.request(storeURL!).responseJSON( completionHandler: { response in
            if let jsonParser = response.result.value as? [String : Any] {
                let parsedStore = StoreParser()
                stores = parsedStore.parseStoreJSON(jsonDict: jsonParser)
            }
            completion(stores)
        }
        )
    }
}
