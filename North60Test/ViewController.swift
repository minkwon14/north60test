//
//  ViewController.swift
//  North60Test
//
//  Created by Minseo Kwon on 2017-12-21.
//  Copyright © 2017 MinKwon. All rights reserved.
//

import UIKit
import GoogleMaps

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var viewMap: GMSMapView!
    @IBOutlet weak var tableView: UITableView!
    
    var mapView: GMSMapView?
    var stores = [Store]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NetworkManager.sharedInstance.getStores(completion: { (stores) in
            self.stores = stores
//            self.addMarker(stores: stores)
//            self.showMap()
            
            
            self.showMap(stores: stores)
            self.tableView.reloadData()
           
        })
    
    }
    
    func showMap (stores: [Store]) {
        let camera = GMSCameraPosition.camera(withLatitude: 43.6532, longitude: -79.3832, zoom: 10.0)
        viewMap.camera = camera
        mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        
        for store in stores {
            var marker = GMSMarker()

            marker.position = CLLocationCoordinate2D(latitude: store.latitude, longitude: store.longitude)
            marker.title = store.name
            marker.map = viewMap
        }
        
        viewMap = mapView
    
    }
    
    func addMarker (stores: [Store]) {
        var marker = GMSMarker()

        for store in stores {
            marker.position = CLLocationCoordinate2D(latitude: store.latitude, longitude: store.longitude)
            marker.title = store.name
            marker.map = viewMap
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return stores.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell", for: indexPath) as! CustomTableViewCell
        
        cell.nameLabel.text = stores[indexPath.row].name
        
        return cell
    }

}

