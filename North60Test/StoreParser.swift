//
//  StoreParser.swift
//  North60Test
//
//  Created by Minseo Kwon on 2017-12-21.
//  Copyright © 2017 MinKwon. All rights reserved.
//

import Foundation

class StoreParser {
    
    func parseStoreJSON(jsonDict: [String : Any]) -> [Store] {
        
        var storesArray = [Store]()
        
        if let storesDict = jsonDict["result"] as? [[String : Any]] {
            for store in storesDict {
                let storeClass = Store()
                if let name  = store["name"] as? String {
                    storeClass.name = name
                }
                if let latitude = store["latitude"] as? Double {
                    storeClass.latitude = latitude
                }
                if let longitude = store["longitude"] as? Double {
                    storeClass.longitude = longitude
                }
                
                storesArray.append(storeClass)
            }
            
        }
        
        return storesArray
    }
}
