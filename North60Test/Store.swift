//
//  Store.swift
//  North60Test
//
//  Created by Minseo Kwon on 2017-12-21.
//  Copyright © 2017 MinKwon. All rights reserved.
//

import Foundation

class Store {
    
    var name = String()
    var latitude = Double()
    var longitude = Double()
}
